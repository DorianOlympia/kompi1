import os
import sys
import re
import codecs


# COUNT UNIQUE!!!


def get_name(content):
    return re.findall(r'(?<=<META NAME="AUTOR" CONTENT=").*(?=")', content)[0]


def get_keywords(content):
    keywords = re.findall(r'(?<=<META NAME="KLUCZOWE)_.*CONTENT=".*"', content)
    res = list()
    for keyword in keywords:
        res.append(re.findall(r'(?<=CONTENT=").*(?=")', keyword))
    res = map(lambda x : x[0], res)
    return filter(lambda x : len(x) > 0, res)

def count_abbr(content):
    abbr = set(re.findall(r'(?<= )[a-zA-Z]{1,3}\.', content))
    return len(abbr)


def count_floats(content):
    result = re.findall(r'(^|(?<=\n))\d*\.\d+((E|e)(\+|-)\d+)?(?=((([\.] )|[\?\!]+)|[^.\w]))', content)
    result.extend(re.findall(r'(^|(?<=\n))\d*,\d+((E|e)(\+|-)\d+)?(?=((([\.,] )|[\?\!]+)|[^,\w]))', content))
    result = set(result)
    return len(result)

def count_emails(content):
    result = re.findall(r'([\w]+(\.\w+)*@[a-zA-Z0-9-]+\.([a-zA-Z]+\.)*[a-z]+)(?=[^.\w])', content)
    result = set(map(lambda x: x[0], result))
    return len(result)


def processFile(filepath):
    fp = codecs.open(filepath, 'rU', 'iso-8859-2')

    content = fp.read()

    fp.close()
    print("nazwa pliku:", filepath)
    print("autor:", get_name(content).encode("utf-8"))
    print("dzial:")
    print("slowa kluczowe:", get_keywords(content))
    content = re.search(r'<P>(.|\n)*?<META', content).group(0)
    print("liczba zdan:")
    print("liczba skrotow:", count_abbr(content))
    print("liczba liczb calkowitych z zakresu int:")
    print("liczba liczb zmiennoprzecinkowych:", count_floats(content))
    print("liczba dat:")
    print("liczba adresow email:", count_emails(content))
    print("\n")



try:
    path = sys.argv[1]
except IndexError:
    print("Brak podanej nazwy katalogu")
    sys.exit(0)


def main():
    tree = os.walk(path)
    for root, dirs, files in tree:
        for f in files:
            if f.endswith(".html"):
                filepath = os.path.join(root, f)
                processFile(filepath)
                # line below to be deleted
                #return

if __name__ == "__main__":
    main()